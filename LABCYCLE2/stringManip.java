import java.util.*;
class  stringManip{
        public static void main(String a[]){
                String s = new String("Hello");
                System.out.println("\n\nValue of string "+s+" after replace operation");
                s = s.replace("e","H");
                System.out.println(s);
                System.out.println("\nValue of string "+s+" after upperCase operation");
                s = s.toUpperCase();
                System.out.println(s);
                StringBuffer s1 = new StringBuffer("StringBuffer");
                System.out.println("\nValue of string "+s1+" after inserting 'a' at position 1\n");
                s1.insert(1,'a');
                System.out.println(s1);
        }
}
