import java.util.*;
class employee{
        int eNo,eSalary;
        String eName;
        employee(int eNo,String eName,int eSalary){
                this.eNo = eNo;
                this.eName = eName;
                this.eSalary = eSalary;
        }
        void display(){
                System.out.println("Name : "+eName);
                System.out.println("Id : "+eNo);
                System.out.println("Salary : "+eSalary);
        }
}

class employeeDetails{
        public static void main(String args[]){
                int n,id,salary;
                String name;
                System.out.println("Enter the number of employee : ");
                Scanner input = new Scanner(System.in);
                n = input.nextInt();
                employee[] e = new employee[n];
                for(int i=0;i<n;i++){
                        System.out.println("\nEnter id of employee "+(i+1)+" : ");
                        id = input.nextInt();
                        System.out.println("Enter name of employee "+(i+1)+" : ");
                        name = input.next();
                        System.out.println("Enter salary of employee "+(i+1)+" : ");
                        salary = input.nextInt();
                        e[i]= new employee(id,name,salary);
                }
                                //searching array of employee
                System.out.println("\nEnter employee id to be searched : ");
                n = input.nextInt();
                for(int i=0;i<n;i++){
                        if(n == e[i].eNo){
                                System.out.println("\n\tDetails of employee "+(i+1));
                                e[i].display();
                                return;
                        }
                }
                System.out.println("\nEmployee with "+n+" not found");
        }
}

