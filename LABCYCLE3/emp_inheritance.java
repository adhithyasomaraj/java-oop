import java.util.*;
class Employee
{
int empid;
String empname;
double esalary;
String address;
Employee(int id,String name,double sal,String add)
{
empid=id;
empname=name;
esalary=sal;
address=add;
}
}
class Teacher extends Employee
{
String deptname;
String subject;
Teacher(int id,String name,double sal,String add,String dname,String sub)
{
super(id,name,sal,add);
deptname=dname;
subject=sub;
}
void display()
{
System.out.println("\t Emp id : "+empid+"\n \t Emp name : "+empname+"\n \t Salary : " +esalary+"\n \t Address : " +address+ "\n \t Department : "+deptname+"\n \tSubject taught:"+subject);

System.out.println();
}
}
class emp
{
public static void main(String args[])
{
int i,n;
Scanner sc=new Scanner(System.in);
System.out.println("Enter no of employees: ");
n=sc.nextInt();
Teacher obj[]=new Teacher[n];
for(i=0;i<n;i++)
{
System.out.println("Enter emp id : ");
int id=sc.nextInt();
System.out.println("Enter emp name : ");
String name=sc.next();
System.out.println("Enter emp salary : ");
Double sal=sc.nextDouble();
System.out.println("Enter emp address : ");
String add=sc.next();
System.out.println("Enter emp dept : ");
String dname=sc.next();
System.out.println("Enter subject taught : ");
String sub=sc.next();
obj[i]=new Teacher(id,name,sal,add,dname,sub);
}
for(i=0;i<n;i++)
{
obj[i].display();
}
}
}
