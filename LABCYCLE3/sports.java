import java.util.*;
interface Student{
void set_value1(String sname,int s);
} interface Sports{
void set_value2(int p);
}
class Result implements Student, Sports{
String name;
int total,score_point;
public void set_value1(String sname,int s){
name=sname;
total=s;
}
public void set_value2(int pt){
score_point=pt;
}
void display(){
System.out.println("Name: " + name);
System.out.println("Academic score: " + total);
System.out.println("Score obtained in sports: " + score_point);
}
}
class sports{
public static void main(String args[]){
Scanner sc=new Scanner(System.in);
int total,pt;
System.out.println("Enter the academic and sports details of a student: ");
System.out.println("Name of the student: ");
String name=sc.nextLine();
System.out.println("Enter the total marks obtained: ");
total=sc.nextInt();
System.out.println("Enter the overall score point obtained in sports: ");
pt=sc.nextInt();
Result obj=new Result();
obj.set_value1(name,total);
obj.set_value2(pt);
obj.display();
}
}