import java.util.*;  
class Mapprgrm{  
	public static void main(String args[]){  
   		HashMap<Integer,String> hm=new HashMap<Integer,String>();    
   		Scanner s=new Scanner(System.in);
		Scanner sc=new Scanner(System.in);
		int k; 
		String val;
		hm.put(101,"Vijay");    
      		hm.put(102,"Rahul");  
      		System.out.println("Initial list of elements:");  
     		for(Map.Entry m:hm.entrySet())   
        		System.out.println(m.getKey()+" "+m.getValue());  
        	System.out.println(" ");   
		int i=1;
		while(i==1){
			System.out.println(" 1.Insertion \n 2.Deletion\n 3.Replace");
			System.out.println("Enter your choice: " );
			int ch=s.nextInt();
			switch(ch){
				case 1: System.out.println("Enter a data as key(integer)- value(string) pair into the given list:  ");
					System.out.print("Key: ");
					k=s.nextInt();
					System.out.print("Value: ");
					val=sc.nextLine();
					hm.put(k,val);
					System.out.println("Updated list of elements: " + hm);  
					break;
				case 2: System.out.print("Enter the key to be deleted: ");
				 	k=s.nextInt();
					hm.remove(k);  
					System.out.println("Updated list of elements: " + hm);
        				break;
				case 3:	System.out.print("Enter the key of the key-value pair to be replaced: ");
					k=s.nextInt();
					System.out.print("Enter the new value: ");
					val=sc.nextLine();
				 	hm.replace(k,val);  
				 	System.out.println("Updated list of elements: " + hm); 
        				break;
			}
			System.out.print("Do you want to continue?(1 or 0)");
			i=s.nextInt();
		}
  
 	}  
} 

