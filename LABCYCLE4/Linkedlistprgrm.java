import java.util.*;
class Linkedlistprgrm{
public static void main(String args[]){
ArrayList<String> fruits=new ArrayList<>();
fruits.add("Kiwi");
fruits.add("Orange");
fruits.add("Apple");
fruits.add("Pineapple");
LinkedList<String> fruits_list=new LinkedList<>(fruits);
System.out.println("List elements are : " + fruits_list);
fruits_list.clear();
System.out.println("List after removing all elements: " + fruits_list);
}
}