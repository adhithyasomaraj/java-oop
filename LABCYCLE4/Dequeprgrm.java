import java.util.*;
public class Dequeprgrm {
	public static void main(String[] args) {
		Deque<String> deque = new LinkedList<String>();
		Scanner s=new Scanner(System.in);
		Scanner sc=new Scanner(System.in);
		int i=1;
		while(i==1){
			System.out.println(" 1.Insertion \n 2.Deletion");
			System.out.println("Enter your choice: " );
			int ch=s.nextInt();
			switch(ch){
				case 1:	System.out.println("Add an element(string) to deque : ");
				        String name=sc.nextLine();
					deque.add(name);
					System.out.println("Add an element(string) to the beginning of deque : ");
				        String first=sc.nextLine();
					deque.addFirst(first);
					System.out.println("Add an element(string) to the end of deque : ");
				        String end=sc.nextLine();
					deque.addLast(end);
					System.out.println("push an element(string) to the  deque : ");
				        String p=sc.nextLine();
					deque.push(p);
					System.out.println("\nDeque contents :");
					Iterator iterator = deque.iterator();
					while (iterator.hasNext())
						System.out.println(" " + iterator.next());
					break;
				case 2: System.out.println("\nPop:" + deque.pop());
					System.out.println("\nDeque after pop:" + deque);
					deque.removeFirst(); 
					deque.removeLast(); 
					System.out.println("\nDeque, after removing " + "first and last elements: " + deque);
					break;
			
			}
			System.out.print("Do you want to continue?(1 or 0)");
			i=s.nextInt();
		}
	}
}
