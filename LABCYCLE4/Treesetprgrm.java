import java.util.*;
class Treesetprgrm{
public static void main(String args[]){
TreeSet set=new TreeSet();
set.add("notebook");
set.add("bag");
set.add("calculator");
set.add("calculator");//treeset contains only unique elements
System.out.println("Set elements are: "+ set);
System.out.println();
System.out.println("Traversing element through Iterator in descending order:");
Iterator i=set.descendingIterator();
while(i.hasNext()){
System.out.println(i.next());
}
}
}