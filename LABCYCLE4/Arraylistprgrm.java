import java.util.*;
class Arraylistprgrm{
public static void main(String args[]){
try{
ArrayList<String> names=new ArrayList<>();
Scanner sc=new Scanner(System.in);
Scanner s=new Scanner(System.in);
int p;
boolean result;
String name;
names.add("J K ROWLING");
names.add("LEO TOLSTOY");
names.add("WILLIAM SHAKESPEARE");
System.out.println("Names of famous authors: " + names + "\n");
System.out.println("*******************Enter the strings inuppercase***************");
int i=1;
while(i==1){
System.out.println(" 1.Insertion at specified position\n 2.Find indexposition of an element\n 3.Get the size of the array list\n 4.Check whether list is empty ornot\n 5.Remove an element by its index");
System.out.println("Enter your choice: " );
int ch=s.nextInt();
switch(ch){
case 1: System.out.println("Enter the name of your favouriteauthor:");
name=sc.nextLine();
System.out.println("Enter the index position:");
p=s.nextInt();
names.add(p-1,name);
System.out.println("Array list after insertion: " +
names);
break;
case 2:System.out.println("Enter the name of an author:");
name=sc.nextLine();
System.out.println("Index position of the given element is :" + " "+ (names.indexOf(name)+1));
break;
case 3: System.out.println("Size of the array list is: " +names.size());
break;
case 4: result=names.isEmpty();
System.out.println("List is empty or not? " + result);
break;
case 5: System.out.println("Enter the index position:");
p=s.nextInt();
names.remove(p-1);
System.out.println("Array list after deletion: " + names);
}
System.out.println("Do you want to continue?(1 or 0)");
i=s.nextInt();
}
}
catch(Exception e){
System.out.println("something went wrong" + e);
}
}
}