import java.util.*;
class Stackprgrm{
public static void main(String args[]){
Scanner n=new Scanner(System.in);
Stack<Integer> numbers=new Stack<>();
numbers.push(12);
numbers.push(14);
numbers.push(48);
numbers.push(90);
System.out.println("List elements are: " + numbers);
System.out.println("Enter the position of the object to be deleted: ");
int pos=n.nextInt();
int top=numbers.remove(pos-1);
System.out.println("Removed object is: " + top);
System.out.println("List after deletion : " + numbers);
}
}