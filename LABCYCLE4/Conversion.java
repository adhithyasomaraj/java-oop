import java.util.*;
public class Conversion{
  	public static void main(String args[]) {
      		Map<String, String> map = new HashMap<>();
      		Scanner s=new Scanner(System.in);
		Scanner sc=new Scanner(System.in);
		int k; 
		String val; 
		map.put("Red","#FF0000");    
      		map.put("Green","#00FF00"); 
      		map.put("Blue","#0000FF"); 
       	 	System.out.println("List contains:  " + map);	
      		Map<String, String> treeMap = new TreeMap<>();
      		treeMap.putAll(map);
      		System.out.println("After conversion (HashMap to TreeMap), the list will be:\n  " + treeMap);
   	}
}

