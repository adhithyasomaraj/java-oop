import java.util.*;
class Userexception extends Exception {
        public Userexception(String msg) {
                super(msg);
        }
}
class Passwdexception extends Exception {
        public Passwdexception(String msg) {
                super(msg);
        }
}
class Exception_prgrm{
        public static void main(String[] args) {
                Scanner s = new Scanner(System.in);
                System.out.print("Enter username: ");
                String username = s.nextLine();
                System.out.print("Enter password :: ");
                String password = s.nextLine();
                int length = username.length();
                try {
                        if(length < 6)
                                throw new Userexception("Username must be greater than 6 characters");
                        else if(!password.equals("abc@123"))
                                throw new Passwdexception("Incorrect password");
                        else
                                System.out.println("Login Successful !!!");
                }
                catch (Userexception e) {
                        System.out.println(e);
                }
                catch (Passwdexception p) {
                        System.out.println(p);
                }
        }
}
